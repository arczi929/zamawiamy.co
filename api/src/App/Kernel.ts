import {Express, json as ExpressJson, Request, Response, NextFunction, urlencoded as ExpressUrlencode} from "express";
import {attachControllers} from "@decorators/express";
import * as mongoose from 'mongoose';
import {RoomController} from './controllers/RoomController';

export class Kernel {
    public init(express: Express) {
        Kernel.mongoSetup();
        express.use(ExpressJson());
        express.use(ExpressUrlencode({extended: false}));
        express.use((req: Request, res: Response, next: NextFunction) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });
        attachControllers(express, [
            RoomController
        ]);
    }

    private static mongoSetup(): void {
        mongoose.Promise = global.Promise;
        mongoose.connect(process.env.MONGODB_URL, {
            useNewUrlParser: true
        });
    }
}