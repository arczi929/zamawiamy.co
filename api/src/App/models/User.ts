import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;
const oAuthTypes = [
    'facebook',
    'google'
];

export const User = new Schema({
    name: {
        type: String,
        default: ''
    },
    email: {
        type: String,
        default: ''
    },
    username: {
        type: String,
        default: ''
    },
    provider: {
        type: String,
        default: ''
    },
    hashed_password: {
        type: String,
        default: ''
    },
    salt: {
        type: String,
        default: ''
    },
    authToken: {
        type: String,
        default: ''
    },
    facebook: {},
    google: {}
});