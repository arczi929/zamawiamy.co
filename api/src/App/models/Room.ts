import * as mongoose from 'mongoose';
import { Restaurant } from "./Restaurant";

const Schema = mongoose.Schema;

export const Room = new Schema({
    name: String,
    password: String,
    restaurant: Restaurant,
    createdAt: {
        type : Date,
        default : Date.now
    },
    expireAt: {
        type : Date
    },
    participants: String,
    menuPositions: String,
    owner: {
        type : Schema.ObjectId,
        ref : 'User'
    }
});