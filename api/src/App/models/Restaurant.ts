import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const Restaurant = new Schema({
    name: {
        type: String
    },
    source: {
        type: String
    },
    link: {
        type: String
    },
});