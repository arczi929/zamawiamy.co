import {Request, Response, Params, Controller, Get, Post, Put} from '@decorators/express';
import * as mongoose from 'mongoose';
import { Room } from '../models/Room';

const RoomModel = mongoose.model('Room', Room);

@Controller('/rooms')
export class RoomController {
    /**
     * @param req
     * @param res
     */
    @Get('/')
    public getLobbyList(@Request() req, @Response() res): void {
        RoomModel.find({})
            .populate('Restaurant')
            .exec((err, rooms) => {
                if (err) {
                    res.send(err);
                }
                res.json(rooms);
            });
    }

    /**
     * @param {e.Request} req
     * @param {e.Response} res
     */
    @Post('/')
    public createLobby(@Request() req, @Response() res): void {
        let newRoom = new RoomModel(req.body);

        newRoom.save((err, room) => {
            if (err) {
                res.send(err);
            }
            res.json(room);
        });
    }

    /**
     * @param res
     * @param lobbyId
     */
    @Get('/:lobbyId')
    public getLobbyById(@Response() res, @Params('lobbyId') lobbyId: string): void {
        RoomModel.findById(lobbyId, (err, room) => {
            if (err) {
                res.send(err);
            }
            res.json(room);
        });
    }

    /**
     * Nie tak to będzie wyglądać...
     * @param req
     * @param lobbyId
     * @param res
     */
    @Put('/:lobbyId')
    public updateLobby(@Request() req, @Response() res, @Params('lobbyId') lobbyId: string): void {
        RoomModel.findOneAndUpdate({ _id: req.params.lobbyId }, req.body, {new: true},
            (err, room) => {
                if (err) {
                    res.send(err);
                }
                res.json(room);
        });
    }
}