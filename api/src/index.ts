import * as express from 'express';
import {Kernel} from "./App/Kernel";


const app = express();

const kernel = new Kernel();

kernel.init(app);

app.listen(process.env.APP_PORT, () => {
    console.log(`[${new Date().toLocaleTimeString()}] Express server listening on port ${process.env.APP_PORT}`);
});