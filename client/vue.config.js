module.exports = {
  pwa: {
    name: 'zamawiamy.co'
  },

  pluginOptions: {
    i18n: {
      locale: 'pl',
      fallbackLocale: 'pl',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
}
